var contacts = [{
    id: 0,
    name: 'Piotr',
    surname: 'Nowak',
    phone: 123456789,
    email: 'abc@cba.pl',
    face: 'http://www.bartlomiej-zmudzinski.pl/magisterka/faces/1.jpg'
}, {
    id: 1,
    name: 'Jan',
    surname: 'Kowalski',
    phone: 111222333,
    email: '',
    face: 'http://www.bartlomiej-zmudzinski.pl/magisterka/faces/2.jpg'
}, {
    id: 2,
    name: 'Adam',
    surname: 'Parzybut',
    phone: 222444666,
    email: '',
    face: 'http://www.bartlomiej-zmudzinski.pl/magisterka/faces/3.jpg'
}, {
    id: 3,
    name: 'Stefan',
    surname: 'Sosnowski',
    phone: 999888777,
    email: 'a@a.aa',
    face: 'http://www.bartlomiej-zmudzinski.pl/magisterka/whois.png'
}, {
    id: 4,
    name: 'Krzysztof',
    surname: 'Tokarek',
    phone: 987654321,
    email: 'xyz@wp.pl',
    face: 'http://www.bartlomiej-zmudzinski.pl/magisterka/faces/4.jpg'
}, {
    id: 5,
    name: 'Mateusz',
    surname: 'Perka',
    phone: 555000555,
    email: 'zzz@gmail.com',
    face: 'http://www.bartlomiej-zmudzinski.pl/magisterka/faces/5.jpg'
}];
// Save contacts list in Session Storage
$(document).on('pageinit', '#contacts-tab-list', function () {
    if (typeof(Storage) != "undefined" && sessionStorage.getItem('contacts') == null) {
        sessionStorage.setItem('contacts', JSON.stringify(contacts));
    }
    //window.location.hash = 'contacts-tab-list';
    //$.mobile.changePage(window.location.href, {transition: 'slideup'});
});
// Create Contacts Listview
$(document).on('pagebeforeshow', '#contacts-tab-list', function () {
    if (typeof(Storage) != "undefined" && sessionStorage.getItem('contacts')) {
        //console.log(sessionStorage.getItem('contacts'));
        $('#contacts-list').html('');
        $.each(JSON.parse(sessionStorage.getItem('contacts')), function (key, value) {
            var contact = value.name + ' ' + value.surname;
            $('#contacts-list').append('<li id="' + value.id + '"><a href="#detail" data-transition="slide"><div class="avatar-container"></div><img class="avatar" src="' + value.face + '"/></div><h3>' + contact + '</h3></a></li>');
        });
        $('#contacts-list').listview('refresh');
    }
});
// Handle event on click listview item
$(document).on('pageshow', '#contacts-tab-list', function () {
    $('#contacts-list li').on('click', function (event) {
        event.preventDefault();
        var id = $(this).attr('id');
        sessionStorage.setItem('id', id);
        $.mobile.changePage("#detail", {transition: 'slide'});
    });
});
// Show detail of contact
$(document).on('pagebeforeshow', "#detail", function () {
    var id = sessionStorage.getItem('id');
    var contact = getContactById(id);
    var desceripion = '<img src="' + contact.face + '" class="img-circle">'
        + '<div class="ui-content">'
        + '<h3 style="margin: 0;">'+contact.name+' '+contact.surname+'</h4>'
        + '<p style="margin: 0; margin-bottom: .6em; font-size: .8em;">Imię i nazwisko</p>'
        + '<h3 style="margin: 0;">'+contact.phone+'</h4>'
        + '<p style="margin: 0; margin-bottom: .6em; font-size: .8em;">Numer telefonu</p>';
    if (contact.email) {
        desceripion += '<h3 style="margin: 0;">'+contact.email+'</h4>';
        desceripion += '<p style="margin: 0; margin-bottom: .6em; font-size: .8em;">E-mail</p></div>';
    } else {
        desceripion += '</div>';
    }
    $('h1#contact-title').text(contact.name + ' ' + contact.surname);
    $('#detail-container').html(desceripion);
});
// Show contact form
$(document).on('pagebeforeshow', "#form", function (event, data) {
	//Edit contact
	if (data.prevPage.attr('id') == 'detail') {
		$('#form-title').text('Edytuj kontakt');
		var id = sessionStorage.getItem('id');
		var contact = getContactById(id);
		$('#contact-form input#name').val(contact.name);
		$('#contact-form input#surname').val(contact.surname);
		$('#contact-form input#phone').val(contact.phone);
		$('#contact-form input#email').val(contact.email);
		$('#contact-form input#id').val(contact.id);
		$('#contact-form input#face').val(contact.face);
	}
	//New contact
	else {
		$('#form-title').text('Nowy kontakt');
		$('#contact-form').clearForm();
	}
	
	$('#contact-form').validate({
		rules: {
			name: {
				required: true,
				minlength: 3,
				maxlength: 30
			},
			surname: {
				required: true,
				minlength: 3,
				maxlength: 30
			},
			phone: {
				required: true,
				pattern: /^\d{9}$/
			},
			email: {
				email: true
			}
		},
		messages: {
			name: {
				required: "Imię jest wymagane!",
				minlength: "Imię musi mieć przynajmniej 3 znaki!",
				maxlength: "Imię może mieć maksymalnie 30 znaków!"
			},
			surname: {
				required: "Nazwisko jest wymagane!",
				minlength: "Nazwisko musi mieć przynajmniej 3 znaki!",
				maxlength: "Nazwisko może mieć maksymalnie 30 znaków!"
			},
			phone: {
				required: "Numer telefonu jest wymagany!",
				pattern: "Numer telefonu musi zawierać 9 cyfr!"
			},
			email: {
				email: "Adres e-mail jest niepoprawny!"
			}
		},
		errorPlacement: function (error, element) {
			error.appendTo(element.parent().prev());
		},
		submitHandler: function (form) {
				var contact = {
					id: $('#contact-form input#id').val(),
					name: $('#contact-form input#name').val(),
					surname: $('#contact-form input#surname').val(),
					phone: $('#contact-form input#phone').val(),
					email: $('#contact-form input#email').val(),
					face: $('#contact-form input#face').val()
				};
				console.log(contact);
				saveContact(contact);
				$.mobile.changePage("#contacts-tab-list", {transition: 'pop'});
		}
	});
	
});
// Handle remove contact
$(document).on('pageshow', '#remove-dialog', function () {
    $('a#remove-contact').on('click', function (event) {
        event.preventDefault();
        var id = sessionStorage.getItem('id');
        var tmp = [];
        $.each(JSON.parse(sessionStorage.getItem('contacts')), function (key, value) {
            if (value.id != id) {
                tmp.push(value);
            }
        });
        sessionStorage.setItem('contacts', JSON.stringify(tmp));
        $.mobile.changePage("#contacts-tab-list", {transition: 'pop'});
    });
});
// Get contact by id
function getContactById(id) {
    var contact = null;
    $.each(JSON.parse(sessionStorage.getItem('contacts')), function (key, value) {
        if (value.id == id) {
            contact = value;
        }
    });
    return contact;
}
//Save contact
function saveContact(contact) {
    var tmp = [];
    if (contact.id == null || contact.id == '') {
        var con = contact;
        var id = 0;
        $.each(JSON.parse(sessionStorage.getItem('contacts')), function (key, value) {
            tmp.push(value);
            if (value.id > id)
                id = value.id;
        });
        con.id = id + 1;
        con.face = 'http://www.bartlomiej-zmudzinski.pl/magisterka/whois.png';
        tmp.push(con);
    }
    else {
        $.each(JSON.parse(sessionStorage.getItem('contacts')), function (key, value) {
            if (contact.id == value.id)
                tmp.push(contact);
            else
                tmp.push(value);
        });
    }
    sessionStorage.setItem('contacts', JSON.stringify(tmp));
}
// Clear contact form
$.fn.clearForm = function () {
    return this.each(function () {
        var type = this.type, tag = this.tagName.toLowerCase();
        if (tag == 'form')
            return $(':input', this).clearForm();
        if (type == 'text' || type == 'password' || type == 'tel' || type == 'hidden')
            this.value = '';
        else if (type == 'checkbox' || type == 'radio')
            this.checked = false;
        else if (tag == 'select')
            this.selectedIndex = -1;
    });
};