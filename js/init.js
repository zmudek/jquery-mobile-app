var contacts = [{
    id: 0,
    name: 'Piotr',
    surname: 'Nowak',
    phone: 123456789,
    email: 'abc@cba.pl',
    face: 'https://pbs.twimg.com/profile_images/514549811765211136/9SgAuHeY.png'
}, {
    id: 1,
    name: 'Jan',
    surname: 'Kowalski',
    phone: 111222333,
    email: '',
    face: 'https://avatars3.githubusercontent.com/u/11214?v=3&s=460'
}, {
    id: 2,
    name: 'Adam',
    surname: 'Parzybut',
    phone: 222444666,
    email: '',
    face: 'https://pbs.twimg.com/profile_images/479090794058379264/84TKj_qa.jpeg'
}, {
    id: 3,
    name: 'Stefan',
    surname: 'Sosnowski',
    phone: 999888777,
    email: 'a@a.aa',
    face: ''
}, {
    id: 4,
    name: 'Krzysztof',
    surname: 'Tokarek',
    phone: 987654321,
    email: 'xyz@wp.pl',
    face: 'https://pbs.twimg.com/profile_images/578237281384841216/R3ae1n61.png'
}, {
    id: 5,
    name: 'Mateusz',
    surname: 'Perka',
    phone: 555000555,
    email: 'zzz@gmail.com',
    face: 'https://pbs.twimg.com/profile_images/598205061232103424/3j5HUXMY.png'
}];

$(document).on('pageinit', '#contacts-tab-list', function () {
    if (typeof(Storage) != "undefined" && sessionStorage.getItem('contacts') == null) {
        sessionStorage.setItem('contacts', JSON.stringify(contacts));
    }

    //window.location.hash = 'contacts-tab-list';
    //$.mobile.changePage(window.location.href, {transition: 'slideup'});
});

$(document).on('pageshow', '#contacts-tab-list', function () {
    var screen = $.mobile.getScreenHeight();
    var header = $(".ui-header").hasClass("ui-header-fixed") ? $(".ui-header").outerHeight() - 1 : $(".ui-header").outerHeight();
    var footer = $(".ui-footer").hasClass("ui-footer-fixed") ? $(".ui-footer").outerHeight() - 1 : $(".ui-footer").outerHeight();
    var contentCurrent = $(".ui-content").outerHeight() - $(".ui-content").height();
    var content = screen - header - footer;// - contentCurrent;
    sessionStorage.setItem('contentHeight', content);
});

$(document).on("pagebeforeshow", "#multimedia-audio", function () {
    var div = $('<div style="width: 1em;"></div>').appendTo('body');
    var em = div.width();
    div.remove();
    var screen = $(window).width();
    $('audio').mediaelementplayer({
        audioWidth: screen - (2 * em)
    });
    $('.mejs-audio').css('width', screen - (2 * em) + 'px');
});

$(document).on("pagebeforeshow", "#multimedia-video", function () {
    var div = $('<div style="width: 1em;"></div>').appendTo('body');
    var em = div.width();
    div.remove();
    var screen = $(window).width();
    var contentWidth = (screen - (2 * em)) / 16;
    $('video').width(contentWidth * 16);
    $('video').height(contentWidth * 9);
    $('video').mediaelementplayer();
});

$(document).on("pageshow", "#map-tab", function () {
    if (typeof(Storage) != "undefined" && sessionStorage.getItem('contentHeight')) {
        $('#map-canvas').height(sessionStorage.getItem('contentHeight'));
    } else {
        $('#map-canvas').height($(window).height());
    }
    var defaultPos = new google.maps.LatLng(51.1086726, 17.0601578);  //PWr Location
    if (navigator.geolocation) {
        console.log("Urządzenie wspiera geolokalizację!");
        function success(pos) {
            drawMap(new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude));
        }

        function fail(error) {
            drawMap(defaultPos);
        }

        // Find the users current position.  Cache the location for 5 minutes, timeout after 1 seconds
        navigator.geolocation.getCurrentPosition(success, fail, {
            maximumAge: 500000,
            enableHighAccuracy: true,
            timeout: 1000
        });
    } else {
        console.log("Urządzenie nie wspiera geolokalizacji...");
        drawMap(defaultPos);
    }
    function drawMap(pos) {
        var mapOptions = {
            zoom: 12,
            center: pos,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
        var marker = new google.maps.Marker({
            position: pos,
            map: map,
            animation: google.maps.Animation.BOUNCE
        });
    }
});

$(document).on("pageshow", "#weather-tab", function () {
    $('#check-weather-btn').on('click', function (event) {
        event.preventDefault();
        if ($('#city').val()) {
            console.log($('#city').val());
            sessionStorage.setItem('city', $('#city').val());
            $.mobile.changePage("#weather-actual", {transition: 'slide'});
            $('#city-error').hide();
        } else {
            $('#city-error').show();
        }
    });
});


$(document).on("pagebeforeshow", "#weather-actual", function () {
    $.ajax({
        type: 'GET',
        url: 'http://api.openweathermap.org/data/2.5/weather?',
        data: {
            appid: 'd9c18ced2f48846634dcde751ec21134',
            q: sessionStorage.getItem('city')
        },
        dataType: 'json',
        success: function (data) {
            $('#city-txt').text(data.name+', '+data.sys.country);
            $('#weather-icon').html('<img src="http://openweathermap.org/img/w/' + data.weather[0].icon + '.png"/>');
            $('#temp-txt').text(Math.round(parseFloat(data.main.temp) - 273.15)+'°');
            $('#humidity-txt').text(data.main.humidity+'%');
            $('#pressure-txt').text(Math.round(parseFloat(data.main.pressure))+'hPa');
        },
        error: function () {
            alert("Wystąpił nieoczekiwany błąd podczas pobierania danych...");
        }
    });
});

$(document).on("pagebeforeshow", "#weather-forecast", function () {
    $.ajax({
        type: 'GET',
        url: 'http://api.openweathermap.org/data/2.5/forecast/daily?',
        data: {
            appid: 'd9c18ced2f48846634dcde751ec21134',
            mode: 'xml',
            units: 'metric',
            cnt: 16,
            q: sessionStorage.getItem('city')
        },
        dataType: 'xml',
        success: function (xml) {
            $(xml).find('time').each(function() {
                $('#forecast-city').text(sessionStorage.getItem('city') + ' - Prognoza na 16 dni');
                var temp = $($(this).find('temperature')[0]).attr('day');
                var icon = $($(this).find('symbol')[0]).attr('var');
                console.log(temp);
                $('#forecast-list').append('<li><div class="row">'
                + '<div><h3>' + $(this).attr('day') + '</h3></div>'
                + '<div style="text-align: right; margin: 0;"><img src="http://openweathermap.org/img/w/' + icon + '.png"/></div>'
                + '<div style="text-align: center;"><h3>' + Math.round(temp) + '°</h3></div>'
                + '</div></li>');
            });
            $('#forecast-list').listview('refresh');
        },
        error: function () {
            alert("Wystąpił nieoczekiwany błąd podczas pobierania danych...");
        }
    });
});