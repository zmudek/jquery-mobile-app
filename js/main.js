// Helped function measure content height and save it in Session Storage
$(document).on('pageshow', '#contacts-tab-list', function () {
    var screen = $.mobile.getScreenHeight();
    var header = $(".ui-header").hasClass("ui-header-fixed") ? $(".ui-header").outerHeight() - 1 : $(".ui-header").outerHeight();
    var footer = $(".ui-footer").hasClass("ui-footer-fixed") ? $(".ui-footer").outerHeight() - 1 : $(".ui-footer").outerHeight();
    var contentCurrent = $(".ui-content").outerHeight() - $(".ui-content").height();
    var content = screen - header - footer;// - contentCurrent;
    sessionStorage.setItem('contentHeight', content);
});
//
// WEATHER TAB
//
// Handle input on Weaher Tab
$(document).on("pageshow", "#weather-tab", function () {
    $('#check-weather-btn').on('click', function (event) {
        event.preventDefault();
        if ($('#city').val()) {
            console.log($('#city').val());
            sessionStorage.setItem('city', $('#city').val());
            $.mobile.changePage("#weather-actual", {transition: 'slide'});
            $('#city-error').hide();
        } else {
            $('#city-error').show();
        }
    });
});
// Get actual weather
$(document).on("pagebeforeshow", "#weather-actual", function () {
    $.ajax({
        type: 'GET',
        url: 'http://api.openweathermap.org/data/2.5/weather?',
        data: {
            appid: 'd9c18ced2f48846634dcde751ec21134',
            q: sessionStorage.getItem('city')
        },
        dataType: 'json',
        success: function (data) {
            $('#city-txt').text(data.name+', '+data.sys.country);
            $('#weather-icon').html('<img src="http://openweathermap.org/img/w/' + data.weather[0].icon + '.png"/>');
            $('#temp-txt').text(Math.round(parseFloat(data.main.temp) - 273.15)+'°');
            $('#humidity-txt').text(data.main.humidity+'%');
            $('#pressure-txt').text(Math.round(parseFloat(data.main.pressure))+'hPa');
        },
        error: function () {
            alert("Wystąpił nieoczekiwany błąd podczas pobierania danych...");
        }
    });
});
// Get forecast weather
$(document).on("pagebeforeshow", "#weather-forecast", function () {
    $.ajax({
        type: 'GET',
        url: 'http://api.openweathermap.org/data/2.5/forecast/daily?',
        data: {
            appid: 'd9c18ced2f48846634dcde751ec21134',
            mode: 'xml',
            units: 'metric',
            cnt: 16,
            q: sessionStorage.getItem('city')
        },
        dataType: 'xml',
        success: function (xml) {
            $(xml).find('time').each(function() {
                $('#forecast-city').text(sessionStorage.getItem('city') + ' - Prognoza na 16 dni');
                var temp = $($(this).find('temperature')[0]).attr('day');
                var icon = $($(this).find('symbol')[0]).attr('var');
                console.log(temp);
                $('#forecast-list').append('<li><div class="row">'
                + '<div><h3>' + $(this).attr('day') + '</h3></div>'
                + '<div style="text-align: right; margin: 0;"><img src="http://openweathermap.org/img/w/' + icon + '.png"/></div>'
                + '<div style="text-align: center;"><h3>' + Math.round(temp) + '°</h3></div>'
                + '</div></li>');
            });
            $('#forecast-list').listview('refresh');
        },
        error: function () {
            alert("Wystąpił nieoczekiwany błąd podczas pobierania danych...");
        }
    });
});
//
// GALLERY TAB
//
// Create gallery
$(document).on('pagebeforeshow', '#gallery-tab', function () {
    for (var i = 1; i <= 20; i++) {
        var n = i.toString();
        if (i < 10) {
            n = '0' + n;
        }
        var photo = {
            src: 'http://www.bartlomiej-zmudzinski.pl/magisterka/gallery/' + n + '.jpg',
            min: 'http://www.bartlomiej-zmudzinski.pl/magisterka/gallery/' + n + '_min.jpg'
        };
        $('#photos').append('<a href="' + photo.src + '"><img id="' + n + '" src="' + photo.min + '" class="thumbnail" /></a>');
    }
    $('#gallery-tab').trigger('pagecreate');
});
// Handle gallery
$(document).on('pageshow', '#gallery-tab', function(){
    var myPhotoSwipe = $('#photos a').photoSwipe({
        jQueryMobile: true,
        loop: false,
        enableMouseWheel: false,
        enableKeyboard: true
    });
    //myPhotoSwipe.show(0);
});
//
// MULTIMEDIA TAB
//
// Set width of audio player
$(document).on("pagebeforeshow", "#multimedia-audio", function () {
    var div = $('<div style="width: 1em;"></div>').appendTo('body');
    var em = div.width();
    div.remove();
    var screen = $(window).width();
    $('audio').mediaelementplayer({
        audioWidth: screen - (2 * em)
    });
    $('.mejs-audio').css('width', screen - (2 * em) + 'px');
});
// Set size of video player
$(document).on("pagebeforeshow", "#multimedia-video", function () {
    var div = $('<div style="width: 1em;"></div>').appendTo('body');
    var em = div.width();
    div.remove();
    var screen = $(window).width();
    var contentWidth = (screen - (2 * em)) / 16;
    $('video').width(contentWidth * 16);
    $('video').height(contentWidth * 9);
    $('video').mediaelementplayer();
});
//
// MAP TAB
//
// Show map and actual location
$(document).on("pageshow", "#map-tab", function () {
    if (typeof(Storage) != "undefined" && sessionStorage.getItem('contentHeight')) {
        $('#map-canvas').height(sessionStorage.getItem('contentHeight'));
    } else {
        $('#map-canvas').height($(window).height());
    }
    var defaultPos = new google.maps.LatLng(51.1086726, 17.0601578);  //PWr Location
    if (navigator.geolocation) {
        console.log("Urządzenie wspiera geolokalizację!");
        function success(pos) {
            drawMap(new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude));
        }

        function fail(error) {
            drawMap(defaultPos);
        }

        // Find the users current position.  Cache the location for 5 minutes, timeout after 1 seconds
        navigator.geolocation.getCurrentPosition(success, fail, {
            maximumAge: 500000,
            enableHighAccuracy: true,
            timeout: 1000
        });
    } else {
        console.log("Urządzenie nie wspiera geolokalizacji...");
        drawMap(defaultPos);
    }
    function drawMap(pos) {
        var mapOptions = {
            zoom: 12,
            center: pos,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
        var marker = new google.maps.Marker({
            position: pos,
            map: map,
            animation: google.maps.Animation.BOUNCE
        });
    }
});