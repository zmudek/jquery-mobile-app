$(document).on('pagebeforeshow', '#gallery-tab', function () {
    for (var i = 1; i <= 20; i++) {
        var n = i.toString();
        if (i < 10) {
            n = '0' + n;
        }
        var photo = {
            src: 'http://www.bartlomiej-zmudzinski.pl/magisterka/gallery/' + n + '.jpg',
            min: 'http://www.bartlomiej-zmudzinski.pl/magisterka/gallery/' + n + '_min.jpg'
        };
        $('#photos').append('<a href="' + photo.src + '"><img id="' + n + '" src="' + photo.min + '" class="thumbnail" /></a>');
    }
    $('#gallery-tab').trigger('pagecreate');
});

//$(document).on('pageshow', '#gallery-tab', function () {
//    $('#photos img').on('click', function (event) {
//        event.preventDefault();
//        console.log($(this).attr('id'));
//        //$.mobile.loading('show', {
//        //    theme: 'b',
//        //    textonly: 'false',
//        //    textvisible: 'true',
//        //    inline: 'true',
//        //    msgtext: 'Ładuję obraz...'
//        //});
//        $('#overlay').fadeIn();
//        $('#photo').hide();
//        //$('#photo-popup').html('<img src="' + $(this).data('fullimage') + '"/>');
//        $('#photo').attr('src', $(this).data('fullimage')).load(function () {
//            $('#overlay').fadeOut();
//            $('#photo').fadeIn();
//            $('#photo-popup').popup({positionTo: 'window'}).popup('open');//.on("popupafteropen", function () {
//            //    console.log('aa');
//            //    $('#photo-popup').popup('reposition', 'positionTo: window');
//            //});
//        });
//
//    });
//});

$(document).on('pageshow', '#gallery-tab', function(){
    var myPhotoSwipe = $('#photos a').photoSwipe({
        jQueryMobile: true,
        loop: false,
        enableMouseWheel: false,
        enableKeyboard: true
    });
    //myPhotoSwipe.show(0);
});